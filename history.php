<?php
session_start();
if (!$_SESSION) {
    header('location:login.php'); // ini untuk redirect ke halaman login kalo usernya belum login
}

include('./koneksi.php'); // ini untuk koneksi database php

$sql    = "SELECT * FROM gps_log "; // ini query stringnya yang bakal di eksekusi di database
$limit = 10; // ini limit data yang mau ditampilin per halaman.

//[START] kalo di url nya ada ?page=x artinya data yang diambil bakal halaman ke 2 didatabasenya
if (isset($_GET['page'])) { 
    $page = intval($_GET['page']);
    if ($_GET['page'] == 1) {
        $offset = 0;
    } else {
        $offset = $_GET['page'] * $limit;
    }
} else {
    $page = 1;
    $offset = 0;
}
//[END] kalo di url nya ada ?page=x artinya data yang diambil bakal halaman ke 2 didatabasenya

// eksekusi querynya...
$sql    = $sql . ' LIMIT ' . $limit . ' OFFSET ' . $offset;
$result = mysqli_query($conn, $sql);
$historyData = [];

// masukin data dari database ke array $historyData;
while ($data = mysqli_fetch_array($result)) {
    $historyData[] = $data;
}

// ini query untuk ngambil jumlah data history yang ada di database
$countQuery = "SELECT count(*) from gps_log";
$result     = mysqli_query($conn, $countQuery);
while ($data = mysqli_fetch_array($result)) {
    $count = $data[0];
}

// Jumlah halaman yang bakal ditampilkan 
// jumlah halaman = jumlah data / jumlah data yang mau ditampilkan per halaman
$totalPage       = intval($count / $limit);

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>History</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="assets/css/Header-Dark.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <style>
        #mapid {
            height: 500px;
        }
    </style>
</head>

<body>
    <div>
        <div class="header-dark">
            <nav class="navbar fixed-top navbar-dark navbar-expand-lg navbar-light bg-dark">
                <?php include './navbar.php' ?>
            </nav>
            <br><br>
            <div class="container hero">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <h1 class="text-center">History Lokasi</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container mt-2">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Jam</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Menampilkan data di HTML -->
                                    <?php foreach ($historyData as $hd) : ?>
                                        <tr>
                                            <td><?php echo date('d M Y', strtotime($hd['timestamp'])) ?></td>
                                            <td><?php echo date('H:i:s', strtotime($hd['timestamp'])) ?></td>
                                            <td class="text-center">
                                                <?php
                                                echo '<button class="view-map btn btn-primary btn-sm"
                                                    data-id="' . $hd['id'] . '"
                                                    data-latitude="' . $hd['latitude'] . '"
                                                    data-longitude="' . $hd['longitude'] . '"
                                                    data-timestamp="' . $hd['timestamp'] . '"
                                                    data-altitude="' . $hd['altitude'] . '"
                                                    >Lihat Map</button>';
                                                ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="float-right">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <?php for($i = 1;$i<=$totalPage;$i++): ?>
                                            <li class="page-item"><a class="page-link" href="history.php?page=<?= $i ?>"> <?= $i; ?> </a></li>
                                        <?php endfor;?>

                                        <!-- <?php if($totalPage > 1) : ?>
                                            <?php if($page != 1) : ?>
                                            <li class="page-item"><a class="page-link" href="history.php?page=<?= $page - 1; ?>"><< Sebelumnya </a></li>
                                            <?php endif; ?>
                                            <li class="page-item"><a class="page-link" href="history.php?page=<?= $page + 1 ?>">Selanjutnya >></a></li>
                                        <?php endif; ?> -->
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-map" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Lokasi gajah</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="mapid"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var mymap = L.map('mapid').setView([51.505, -0.09], 15);
        var markerDevice;
        var markerGajah = L.marker([51, 51]).addTo(mymap);

        // fungsi untuk mengambil lokasi kita
        function showPosition(position) {
            var deviceLat = position.coords.latitude;
            var deviceLong = position.coords.longitude;
            deviceGeoloc = [deviceLat, deviceLong];
            markerDevice = L.marker(deviceGeoloc).addTo(mymap);
            markerDevice.bindTooltip("Lokasi anda").openTooltip();
        }

        // fungsi untuk mengambil lokasi kita
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        $(document).ready(function() {
            // ketika button lihat map di click bakal mengeeksekusi fungsi ini
            $(".view-map").click(function() {
                data = $(this).data();
                // untuk ngambil data tanggal dan jam nya
                timestampFormatted = moment(data.timestamp).format("DD MMMM YYYY HH:mm:ss");

                $("#modal-map").modal('show');

                textMarkerGajah = "Lokasi Gajah <br> \
                <b>Waktu :</b>  " + timestampFormatted + "<br>  \
                <b>Latitude</b> : " + data.latitude + "<br> \
                <b>Longitude</b> : " + data.longitude + "<br> \
                <b>Altitude</b> : " + data.altitude + "<br> \
                "

                // untuk menggambar peta nya.
                markerGajah.setLatLng([data.latitude, data.longitude])
                markerGajah.bindTooltip(textMarkerGajah).openTooltip();
                mymap.setView([data.latitude, data.longitude]);
            });
        })

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'your.mapbox.access.token'
        }).addTo(mymap);
    </script>

</body>

</html>
